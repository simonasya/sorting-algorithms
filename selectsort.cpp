#include <iostream>
using namespace std;

/**
 * \brief Swap function for select sort
 */

void swapValues(int * arr, int min, int i){
	int tmp = arr[min];
	arr[min] = arr[i];
	arr[i] = tmp;
}

/**
 * \brief Swap function for select sort for floating point numbers
 */

void swapValues(double * arr, int min, int i){
	double tmp = arr[min];
	arr[min] = arr[i];
	arr[i] = tmp;
}

/**
 * \brief Select sort implementation
 * 
 * \param arr array to sort
 * \param arrSize size of the array
 */

void selectSort(int * arr, int arrSize){
	int sorted = 0, min, i;
	while(sorted < arrSize){
		min = arrSize - 1;
		for(i = sorted; i < arrSize; i++){
			if(arr[i] < arr[min]) min = i;
		}
		swapValues(arr, min, sorted);
		sorted++;
	}	
}

/**
 * \brief Select sort implementation for floating point numbers
 * 
 * \param arr array to sort
 * \param arrSize size of the array
 */

void selectSort(double * arr, int arrSize){
	int sorted = 0, min, i;
	while(sorted < arrSize){
		min = arrSize - 1;
		for(i = sorted; i < arrSize; i++){
			if(arr[i] < arr[min]) min = i;
		}
		swapValues(arr, min, sorted);
		sorted++;
	}	
}