#include <iostream>
using namespace std;

/**
 * \brief Swap function for quicksort
 */

template <typename T>
void swapVal(T * arr, T x, int y){
	int tmp = arr[x];
	arr[x] = arr[y];
	arr[y] = tmp;
}

/**
 * \brief Implementation of Quicksort algorithm
 * \details recursively sorts array
 * 
 * \param arr array of T elements
 * \param start first available index of array
 * \param end last available index of array
 */

template <typename T>
void quickSort(T * arr, int start, T end){
	if((T)start < end){
		T left = start + 1;
		T right = end;
		T pivot = arr[start];
		while(left < right){
			if(arr[left] <= pivot) left ++;
			else if(arr[right] >= pivot) right --;
				 else swapVal(arr, left, right);
		}
		if(arr[left] < pivot){
			swapVal(arr, left, start);
			left --;
		}
		else{
			left --;
			swapVal(arr, left, start);
		}
		quickSort(arr, start, left);
		quickSort(arr, right, end);

	}
}