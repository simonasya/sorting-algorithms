#include <iostream>
using namespace std;

/**
 * \brief Insert sort algorithm implementation
 * 
 * \param arr array to sort
 * \param arrSize size of array
 */

void insertSort(int * arr, int arrSize){
	int value, j;
	for(int i = 1; i < arrSize; i++){
		value = arr[i];
		for(j = i - 1; j >= 0 && arr[j] > value; j--)
			arr[j + 1] = arr[j];
		arr[j + 1] = value;
	}
}

/**
 * \brief Insert sort algorithm implementation for floating point numbers
 * 
 * \param arr array to sort
 * \param arrSize size of array
 */

void insertSort(double * arr, int arrSize){
	double value; 
	int j;
	for(int i = 1; i < arrSize; i++){
		value = arr[i];
		for(j = i - 1; j >= 0 && arr[j] > value; j--)
			arr[j + 1] = arr[j];
		arr[j + 1] = value;
	}
}